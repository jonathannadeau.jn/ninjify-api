import * as yup from 'yup';
import { Topic } from '../../types';

const {backEnd, cloud, databases, frontEnd, programmingLanguages} = Topic;

export const ninjifySchema = yup.object({
    x: yup.mixed().oneOf([backEnd, cloud, databases, frontEnd, programmingLanguages]).required(),
});