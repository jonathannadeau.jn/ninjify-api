import { Router } from 'express';
import { TECHNOLOGY_BUZZWORDS, ADJECTIVE_BUZZWORDS, PERSON_TITLES } from '../../constants';
import { Topic } from '../../types';

const ninjifyRouter = Router();

ninjifyRouter.get('/', (req, res) => {
    try {
    
        const topic = req.query.x as Topic | undefined;

        if(!topic) {
            return res.status(400).json({
                error: 'Missing required parameter `x`'
            });
        }
    
        const title = PERSON_TITLES[Math.floor(Math.random() * PERSON_TITLES.length)];
        const adjective = ADJECTIVE_BUZZWORDS[Math.floor(Math.random() * ADJECTIVE_BUZZWORDS.length)];
        const technology = TECHNOLOGY_BUZZWORDS[topic][Math.floor(Math.random() * TECHNOLOGY_BUZZWORDS[topic].length)];

        const ninjaName = `${adjective} ${technology} ${title}`;

        return res.json({
            ninjaName,
        }).status(200);
    } catch (error) {
        console.error(error);

        return res.status(500).json({
            error: 'Internal Server Error',
        });
    }
    
});

export {ninjifyRouter};