import { Request, Response, NextFunction } from 'express';
import{ Schema, ValidationError } from 'yup';

export const validateRequest = <SchemaType>(schema: Schema<SchemaType>) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        const values = {...req?.body, ...req?.params, ...req?.query};
        try {
            await schema.validate(values);
            next();
        } catch (error) {
            console.error(error);
            if(error instanceof ValidationError) {

                return res.status(400).json({
                    error: error.message,
                });
            }

            return res.status(500).json({
                error: 'Internal Server Error',
                
            });
            
        }
    };
};