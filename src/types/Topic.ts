export enum Topic {
    programmingLanguages = 'programmingLanguages',
    frontEnd = 'frontEnd',
    backEnd = 'backEnd',
    databases = 'databases',
    cloud = 'cloud',
}