import dotenv from 'dotenv';
import os from 'os';

dotenv.config();

export const PORT = process.env.PORT || '8000';
export const HOST = process.env.HOST || os.hostname();
export const ALLOWED_ORIGIN = process.env.ALLOWED_ORIGIN || '';