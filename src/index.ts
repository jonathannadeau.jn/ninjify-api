import express from 'express';
import dotenv from 'dotenv';
import morgan from 'morgan';
import helmet from 'helmet';
import cors from 'cors';
import { ALLOWED_ORIGIN, HOST, PORT } from './constants';
import { ninjifyRouter, ninjifySchema } from './routes';
import { validateRequest } from './middlewares';

dotenv.config();


const app = express();

const distDir = __dirname + '/dist/';
app.use(express.static(distDir));
app.use(helmet());
app.use(cors({
    origin: ALLOWED_ORIGIN,
    allowedHeaders: ['Content-Type'],
    credentials: true,
}));
app.use(morgan('short'));

app.use('/ninjify', validateRequest(ninjifySchema), ninjifyRouter);

app.listen(process.env.PORT, () => {
    console.log(`Server listening on ${HOST}:${PORT}`);
});



