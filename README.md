# Ninjify API

## Technologies used

- Node JS / Express JS
- Heroku

## Description

Generate a random ninja name using the endpoint `/ninjify?x=<keyword>`

List of allowed keywords : 
 - programmingLanguages
 - frontEnd
 - backEnd
 - cloud
 - databases

 